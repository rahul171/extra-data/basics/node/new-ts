let i: number = 0;

const indexStr = (value: number): string => `index - ${value}`;

while (i < 10) {
  const str = indexStr(i);
  console.log(str);
  i++;
}

console.log('hello');
